import React, { useState, useEffect } from 'react';

function PopUpManage(props) {
  const [updatedNameValue, setUpdatedNameValue] = useState('');
  const [updatedDescriptionValue, setUpdatedDescriptionValue] = useState('');
  const [updatedPriceValue, setUpdatedPriceValue] = useState('');
  const [updatedStockValue, setUpdatedStockValue] = useState('');

  useEffect(() => {
    if (props.selectedProduct) {
      setUpdatedNameValue(props.selectedProduct.name);
      setUpdatedDescriptionValue(props.selectedProduct.description);
      setUpdatedPriceValue(props.selectedProduct.price);
      setUpdatedStockValue(props.selectedProduct.stock);
    }
  }, [props.selectedProduct]);

  const handleSaveProduct = () => {
    const updatedProduct = {
      ...props.selectedProduct,
      name: updatedNameValue,
      description: updatedDescriptionValue,
      price: updatedPriceValue,
      stock: updatedStockValue,
    };
    props.updateProduct(updatedProduct);
    props.setTrigger(false);
  };

  return props.trigger ? (
    <div className='popup'>
      <div className='popup-content'>
        <div className='popup-header'>
          <button className='close-button' onClick={() => props.setTrigger(false)}>x</button>
        </div>
        <div>
          <h2>Edit Items</h2>
          <label className='form-label'>Name</label>
          <input
            type='text'
            placeholder='Item name'
            value={updatedNameValue}
            onChange={(e) => setUpdatedNameValue(e.target.value)}
          />
          <br/>
          <label className='form-label'>Description</label>
          <input
            type='text'
            placeholder='Item description'
            value={updatedDescriptionValue}
            onChange={(e) => setUpdatedDescriptionValue(e.target.value)}
          />
          <br/>
          <label className='form-label'>Price</label>
          <input
            type='number'
            placeholder='Price'
            min="0"
            value={updatedPriceValue}
            onChange={(e) => setUpdatedPriceValue(e.target.value)}
          />
          <br/>
          <label className='form-label'>Stock</label>
          <input
            type='number'
            placeholder='Stock'
            min="0"
            value={updatedStockValue}
            onChange={(e) => setUpdatedStockValue(e.target.value)}
          />
          <button onClick={handleSaveProduct}>Save Product</button>
        </div>
      </div>
    </div>
  ) : null;
}

export default PopUpManage;
