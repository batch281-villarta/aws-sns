//awsControllers.js
const { SNSClient, PublishCommand, ConfirmSubscriptionCommand } = require('@aws-sdk/client-sns');

module.exports.incomingMessageHandler = async (req, res) => {
    const snsClient = new SNSClient({
        credentials: {
            accessKeyId: process.env.ACCESS_KEY_ID,
            secretAccessKey: process.env.SECRET_ACCESS_KEY,
        },
        region: process.env.REGION
    });

    const { Type, TopicArn, Token } = req.body;
    
    if (!Type) {
        console.log('Invalid message received: Type field is missing or undefined');
        res.sendStatus(400);
        return;
    }

    if (Type === 'SubscriptionConfirmation') {
        const confirmParams = {
            TopicArn,
            Token,
        };

        try {
            const confirmCommand = new ConfirmSubscriptionCommand(confirmParams);
            await snsClient.send(confirmCommand);
    
            console.log('Subscription confirmed');
            res.sendStatus(200);
        } catch (error) {
            console.error('Error confirming subscription:', error);
            res.sendStatus(500);
        }
    } // # 16. Attached here the else statement
};
